; -- ScalesLoader.iss --
; ������������� ScalesLoader.exe

[Setup]
AppName=ScalesLoader
AppVersion=1.0
DefaultDirName={pf}\ScalesLoader
DefaultGroupName= RBS
UninstallDisplayIcon={app}\ScalesLoader.exe
Compression=lzma2
SolidCompression=yes
OutputDir={app}\ScalesLoader
OutputBaseFileName=Setup
SetupLogging = yes
PrivilegesRequired=admin
UsePreviousAppDir = no
DirExistsWarning=auto
UserInfoPage=yes
;WizardImageFile=Image.bmp

[Languages]
Name: "RU"; MessagesFile: "compiler:Languages\Russian.isl"
;Name: "EN"; MessagesFile: "compiler:Languages\Default.isl"

[Registry]
Root: HKLM; Subkey: "Software\CrystalsRu\ScalesLoader"; Flags: uninsdeletekey

[Files]

Source: "{#SourcePath}\*"  ; DestDir: "{app}"  ; Flags: recursesubdirs
#define JavaInstaller32 "jre-8u151-windows-i586.exe"
#define JavaInstaller64 "jre-8u151-windows-x64.exe"
#define PostgreInstaller32 "PostgreSQL_9.6.9_32bit_Setup.exe"
#define PostgreInstaller64 "PostgreSQL_9.6.9_64bit_Setup.exe"
Source: "{#JavaInstaller32}"; DestDir: "{tmp}";
Source: "{#JavaInstaller64}"; DestDir: "{tmp}";
Source: "{#PostgreInstaller32}"; DestDir: "{tmp}";
Source: "{#PostgreInstaller64}"; DestDir: "{tmp}";

[Code]
const
  REQUIRED_JAVA_VERSION = '1.8';
 
 var  isJava32Needed:Boolean;
      isJava64Needed:Boolean;
      PageT:TInputQueryWizardPage;
      SecondLabel: TLabel;
      ServerNameTextBox: TEdit;
      time:Integer;

{�������� ��������� Java}
function isJava32InstallationNeeded(): Boolean;
var
  isJava64InstallationNeeded:String;
  JavaVer32 : String;
  tmpFileName,
  pathJavaExe: String;
  isGoodJavaVersion32,
  isFoundJavaPath: Boolean;
  ExecStdout: AnsiString;
begin
  { ��������� � ������� }
  { ���������� ����������: }
  {   JavaVer }
  {   isGoodJavaVersion }
  Result:= true; 

  if RegQueryStringValue(HKLM, 'SOFTWARE\JavaSoft\Java Runtime Environment','CurrentVersion', JavaVer32) AND (JavaVer32 <> '') then begin 
   isGoodJavaVersion32 := CompareStr(JavaVer32, REQUIRED_JAVA_VERSION) >= 0;
   if isGoodJavaVersion32 then begin 
     Result:= false;
     isJava32Needed:= Result;
     Exit;
     end;
  end;
   
  if IsWin64 then 
     Result:=false;
     isJava32Needed:= Result;
end;
 

function CheckPortOccupied(Port:String):Boolean;
var
  ResultCode: Integer;
begin
  Exec(ExpandConstant('{cmd}'), '/C netstat -na | findstr'+' /C:":'+Port+' "', '', 0,
       ewWaitUntilTerminated, ResultCode);
  if ResultCode <> 1 then 
  begin
    Log('this port('+Port+') is occupied');
    Result := True; 
  end
    else
  begin
    Result := False;
  end;
end;

function AuthenticateDataPage(Page: TWizardPage): Boolean;
begin
    Result :=True;
end;


function CreateDataPage(PreviousPageId: Integer): Integer;

begin
    PageT :=  CreateInputQueryPage(wpWelcome,'��������� �� PostgreSQL Server',
    '�c������� PostgreSQL Server ������ 9.6','') ;

    SecondLabel :=TLabel.Create(PageT);
    With SecondLabel Do
    begin
        Parent :=PageT.Surface;
        Caption :='����� ��� ��������� - postgres,������ - pgsql';
        Left :=ScaleX(12);
        Top :=ScaleY(75);
        Width :=ScaleX(300);
        Height :=ScaleY(17);
    end;

     with PageT do
     begin
         OnNextButtonClick :=@AuthenticateDataPage;
     end;

    PageT.Add('������� ����, �� ������� �� ������ ������������� Postgres: ', False);
    PageT.Values[0] := ExpandConstant('5432');
    Result :=PageT.ID;
  

end;


procedure InitializeWizard();
begin
   CreateDataPage(wpLicense);
end;

function NextButtonClick(Page: Integer): Boolean;
begin
    Result:=true;
    if Page = PageT.ID then begin
      Result:=not CheckPortOccupied(PageT.Values[0]);
      if (Result = false) and (time < 4) then begin
        MsgBox('��������� ���� �����! ������� ������ ����', mbInformation, MB_OK);
        time:=time+1;
      end;
    end;

    if (time >= 4) then
    Result:=true;
end;

function GetParams(Value: string): string;
begin
  Result := PageT.Values[0];
end;



function GetInstallation(Value: string):string;
var DirInstallation:String;
begin
   
  if (not isWin64) then begin
    if RegQueryStringValue(HKLM, 'SOFTWARE\Postgres Professional\X86\PostgreSQL\9.6\Installations\postgresql-9.6-32bit', 'Base Directory', DirInstallation)  then
      begin
      Insert('"',DirInstallation,0);
      Insert('"',DirInstallation,Length(DirInstallation)+1);
      Result:=DirInstallation;
      Log ('The Value is '+DirInstallation);
      end;
  end
  else
  begin
    if RegQueryStringValue(HKLM64, 'SOFTWARE\Wow6432Node\Postgres Professional\X64\PostgreSQL\9.6\Installations\postgresql-9.6', 'Base Directory', DirInstallation)  then
        begin
        Insert('"',DirInstallation,0);
        Insert('"',DirInstallation,Length(DirInstallation)+1);
        Result:=DirInstallation;
        Log ('The Value is '+DirInstallation);
        end;
  end;
end;

{������������ �����}
procedure DeinitializeSetup();
var
  logfilepathname, logfilename, newfilepathname: string;
begin
  logfilepathname := ExpandConstant('{log}');
  logfilename := ExtractFileName(logfilepathname);
  // Set the new target path as the directory where the installer is being run from
  newfilepathname := ExpandConstant('{src}\') + logfilename;

  FileCopy(logfilepathname, newfilepathname, false);
end; 

{�������� ��������� Postgres}
function CheckPostgresInstalled():Boolean;
var DirInstallation:String;
begin
  Result:= false;
  if (isWin64) then begin
    if RegQueryStringValue(HKLM64, 'SOFTWARE\Wow6432Node\Postgres Professional\X64\PostgreSQL\9.6\Installations\postgresql-9.6', 'Base Directory', DirInstallation) then 
      Result:=true;
  end
  else
  begin
    if RegQueryStringValue(HKLM, 'SOFTWARE\Postgres Professional\X86\PostgreSQL\9.6\Installations\postgresql-9.6-32bit', 'Base Directory', DirInstallation) then 
      Result:=true;
  end;
end;

function isJava64InstallationNeeded(): Boolean;
var
  JavaVer64 : String;
  tmpFileName,
  pathJavaExe: String;
  isGoodJavaVersion64,
  isFoundJavaPath: Boolean;
  ResultCode: Integer;
  ExecStdout: AnsiString;
begin
  { ��������� � ������� }
  { ���������� ����������: }
  {   JavaVer }
  {   isGoodJavaVersion }

  Result:=false;

if IsWin64 then
  begin
  Result:= true; 

  if RegQueryStringValue(HKLM64, 'SOFTWARE\JavaSoft\Java Runtime Environment','CurrentVersion', JavaVer64) AND (JavaVer64 <> '') then begin 
   isGoodJavaVersion64 := CompareStr(JavaVer64, REQUIRED_JAVA_VERSION) >= 0;
     if isGoodJavaVersion64 then begin
       Result:= false;
       isJava64Needed:= Result;
       Exit;
     end;
  end;
   
  if IsWin64 then 
     Result:=true;
     isJava64Needed:= Result;
  end;

end;

{��������� ������ Windows}
function isWindows64 ():Boolean;
  begin
  Result:=IsWin64;
end;


{�������� ��������� ����������}
function IsApplicationInstalled: boolean;
begin
  {result := RegKeyExists(HKEY_LOCAL_MACHINE,'Software\CrystalsRu\ScalesLoader');}
end;


{������������� ���������}
function InitializeSetup: boolean;
begin
  result := not IsApplicationInstalled;
  if not result then
    MsgBox('��������� "Scales Loader" ��� �����������! ��������� ��������� �� ����� ��������! ', mbError, MB_OK);
end;

function Java32bitInstalled():Boolean;
var
  JavaVer32 : String;
  isGoodJavaVersion32:Boolean;
begin
  if RegQueryStringValue(HKLM, 'SOFTWARE\JavaSoft\Java Runtime Environment','CurrentVersion', JavaVer32) AND (JavaVer32 <> '') then begin 
     isGoodJavaVersion32 := CompareStr(JavaVer32, REQUIRED_JAVA_VERSION) >= 0;
      if isGoodJavaVersion32 then begin
         Result:= true;
      end;
  end;
  Result:= false;
end;


[Run]

Filename: "{tmp}\{#PostgreInstaller32}"; Flags: runascurrentuser; \
   StatusMsg: "Postgre SQL 32-bit ����������� ������ �� ����������. ���� ���������..."; \
   Check: not isWindows64;

Filename: "{tmp}\{#PostgreInstaller64}"; Flags: runascurrentuser; \
   StatusMsg: "Postgre SQL 64-bit ����������� ������ �� ����������. ���� ���������..."; \
   Check: isWindows64;

Filename: "{tmp}\{#JavaInstaller64}"; Parameters: "SPONSORS=0"; Flags: runascurrentuser; \
   StatusMsg: "Java Runtime Enviroment Version 1.8 64-bit �� �����������. ���� ���������..."; \
   Check: isJava64InstallationNeeded ;

Filename: "{tmp}\{#JavaInstaller32}"; Parameters: "SPONSORS=0"; Flags: runascurrentuser; \
   StatusMsg: "Java Runtime Enviroment Version 1.8 32-bit �� �����������. ���� ���������..."; \
   Check: isJava32InstallationNeeded ;

Filename: "{app}\bigsql\pg_port.bat"; Parameters: "{code:GetInstallation} {code:GetParams}"; Flags: runascurrentuser; \
   StatusMsg: "���� ������ �������� �� 64 - ������ ��..."; \
   Check: CheckPostgresInstalled;

Filename: "{app}\install_service.bat"; Parameters: "install"; Flags: runascurrentuser; \
   StatusMsg: "���� ��������� 32-������ ������..."; \
   Check: not IsWin64;

Filename: "{app}\install_service-64x.bat"; Parameters: "install"; Flags: runascurrentuser; \
   StatusMsg: "���� ��������� 64-������ ������..."; \
   Check: IsWin64;





   
   





